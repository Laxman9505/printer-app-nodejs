/** @format */

(function () {
  const express = require("express");
  const morgan = require("morgan");
  const app = express();
  const cors = require("cors");
  const Jimp = require("jimp");
  const request = require("request");
  const fs = require("fs");
  const { PNG } = require("pngjs");
  const path = require("path");

  var corsOptions = {
    origin: "http://localhost:3000",
    optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
  };
  const bodyParser = require("body-parser");
  app.use(cors(corsOptions));
  app.use(bodyParser.json());
  app.use(morgan());
  const ThermalPrinter = require("node-thermal-printer").printer;
  const PrinterTypes = require("node-thermal-printer").types;

  function createThermalPrinter(ip, port) {
    const printer = new ThermalPrinter({
      type: PrinterTypes.STAR, // Printer type: 'star' or 'epson'
      interface: `tcp://${ip}:${port}`, // Printer interface
      characterSet: "SLOVENIA", // Set character for lines - default: "-"
      removeSpecialCharacters: false,
      replaceSpecialCharacters: true,
      options: {
        // Additional options
        timeout: 5000, // Connection timeout (ms) [applicable only for network printers] - default: 3000
      },
    });

    return printer;
  }

  app.get("/", async (req, res) => {
    res.status(200).send("dsa");
  });

  app.post("/printEftposReciept", async (req, res) => {
    const { printingData } = req.body;
    console.log("printing data11", printingData);

    // printer.alignCenter();
    // printer.println("Hello world");
    // printer.cut();

    try {
      let printer = createThermalPrinter(
        printingData.ipAddress,
        printingData.port
      );

      // Print the order header

      // Print store details and header
      printer.alignCenter();

      printer.println(printingData);

      printer.bold(false);

      // Print and disconnect the printer
      printer.cut();
      printer.execute();
      console.error("Print done!");
      res.status(200).json({ success: "this is working" });
    } catch (error) {
      console.log("Print failed:", error);
      res.status(400).json({ message: "" });
    }
  });

  app.post("/askForSignature", async (req, res) => {
    const { printingData } = req.body;
    console.log("printing data11", printingData);

    // printer.alignCenter();
    // printer.println("Hello world");
    // printer.cut();

    try {
      const ThermalPrinter = require("node-thermal-printer").printer;
      const PrinterTypes = require("node-thermal-printer").types;
      console.log("body data");
      let printer = new ThermalPrinter({
        type: PrinterTypes.EPSON, // Printer type: 'star' or 'epson'
        interface: "tcp://192.168.1.87:9100", // Printer interface// Printer character set - default: SLOVENIA      removeSpecialCharacters: false, // Removes special characters - default: false
        lineCharacter: "=",
        characterSet: "SLOVENIA", // Set character for lines - default: "-"
        options: {
          // Additional options
          timeout: 5000, // Connection timeout (ms) [applicable only for network printers] - default: 3000
        },
      });
      let isConnected = await printer.isPrinterConnected(); // Check if printer is connected, return bool of status
      let execute = await printer.execute(); // Executes all the commands. Returns success or throws error
      // let raw = await printer.raw(Buffer.from("Hello world")); // Print instantly. Returns success or throws error

      console.log(isConnected, "connected to printer");

      // Print the order header

      // Print store details and header
      printer.alignCenter();

      printer.println(printingData);

      printer.bold(false);

      // Print and disconnect the printer
      printer.cut();
      printer.execute();
      console.error("Print done!");
      res.status(200).json({ success: "this is working" });
    } catch (error) {
      console.log("Print failed:", error);
      res.status(400).json({ message: "" });
    }
  });

  app.post("/printBarcodeImage", async (req, res) => {
    const { printingData } = req.body;
    const iterationOfPrint = printingData?.image?.length;
    const imageUrl = printingData?.image?.[0];
    console.log("printing data", printingData);

    try {
      const ThermalPrinter = require("node-thermal-printer").printer;
      const PrinterTypes = require("node-thermal-printer").types;
      console.log("body data");
      let printer = new ThermalPrinter({
        type: PrinterTypes.STAR, // Printer type: 'star' or 'epson'
        interface: "tcp://192.168.1.87:9100", // Printer interface// Printer character set - default: SLOVENIA      removeSpecialCharacters: false, // Removes special characters - default: false
        lineCharacter: "=",
        characterSet: "SLOVENIA", // Set character for lines - default: "-
        removeSpecialCharacters: false,
        replaceSpecialCharacters: true,
        options: {
          // Additional options
          timeout: 5000, // Connection timeout (ms) [applicable only for network printers] - default: 3000
        },
      });
      let isConnected = await printer.isPrinterConnected(); // Check if printer is connected, return bool of status
      let execute = await printer.execute(); // Executes all the commands. Returns success or throws error
      // let raw = await printer.raw(Buffer.from("Hello world")); // Print instantly. Returns success or throws error

      console.log(isConnected, "connected to printer");
      const buffer = fs.readFileSync("./image.png");

      printer.printImage(buffer, { width: "max" }, (err) => {
        if (err) throw err;
        printer.cut();
        printer.execute((err) => {
          if (err) throw err;
          console.log("Image printed successfully");
        });
      });

      console.error("Print done!");
      res.status(200).json({ success: "this is working" });
    } catch (error) {
      console.log("Print failed:", error);
      res.status(400).json({ message: "" });
    }
  });

  app.post("/printKitchen", async (req, res) => {
    const { printingData } = req.body;
    console.log("printing data11", printingData);
    console.log(
      "printing data",
      printingData?.printingInvoiceDetailsResponseViewModels?.[0]
        ?.orderItemsDetailsResponseViewModels
    );

    // printer.alignCenter();
    // printer.println("Hello world");
    // printer.cut();

    try {
      const ThermalPrinter = require("node-thermal-printer").printer;
      const PrinterTypes = require("node-thermal-printer").types;
      console.log("body data");
      let printer = new ThermalPrinter({
        type: PrinterTypes.EPSON, // Printer type: 'star' or 'epson'
        interface: "tcp://192.168.1.87:9100", // Printer interface// Printer character set - default: SLOVENIA      removeSpecialCharacters: false, // Removes special characters - default: false
        lineCharacter: "=",
        characterSet: "SLOVENIA", // Set character for lines - default: "-"
        options: {
          // Additional options
          timeout: 5000, // Connection timeout (ms) [applicable only for network printers] - default: 3000
        },
      });
      let isConnected = await printer.isPrinterConnected(); // Check if printer is connected, return bool of status
      let execute = await printer.execute(); // Executes all the commands. Returns success or throws error
      // let raw = await printer.raw(Buffer.from("Hello world")); // Print instantly. Returns success or throws error

      console.log(isConnected, "connected to printer");

      // Print the order header
      const storeName =
        printingData?.printingInvoiceDetailsResponseViewModels?.[0]?.storeName;
      const storeAddress =
        printingData?.printingInvoiceDetailsResponseViewModels?.[0]
          ?.storeAddress;
      const date =
        printingData?.printingInvoiceDetailsResponseViewModels?.[0]?.date;
      const orderNumber =
        printingData?.printingInvoiceDetailsResponseViewModels?.[0]
          ?.orderNumber;
      const channel =
        printingData?.printingInvoiceDetailsResponseViewModels?.[0]?.channel;
      const tableNumber =
        printingData?.printingInvoiceDetailsResponseViewModels?.[0]
          ?.tableNumber;
      const customerName =
        printingData?.printingInvoiceDetailsResponseViewModels?.[0]
          ?.customerName;
      const orderType =
        printingData?.printingInvoiceDetailsResponseViewModels?.[0]?.orderType;
      const paymentMethod =
        printingData?.printingInvoiceDetailsResponseViewModels?.[0]
          ?.paymentMethod;
      const taxAmount =
        printingData?.printingInvoiceDetailsResponseViewModels?.[0]?.tax;
      const totalPrice =
        printingData?.printingInvoiceDetailsResponseViewModels?.[0]
          ?.totalAmount;

      // Print store details and header
      printer.alignCenter();
      printer.bold(true);
      printer.setTextSize(1, 1);
      printer.println(`  ${storeName}`);
      printer.setTextNormal();

      printer.println(`  ${storeAddress}`);
      printer.println(`  ${date}`);
      if (printingData?.isPrintFromPayScreen) {
        printer.println(`  Reciept`);
      }
      if (
        !printingData?.isSendToKitchen &&
        !printingData?.isPrintFromPayScreen
      ) {
        printer.println(`  Tax Invoice`);
      }

      printer.newLine();
      printer.newLine();

      // Print order details
      printer.alignLeft();

      printer.println(`Order No.: ${orderNumber}`);
      if (channel) {
        printer.println(`Server: ${channel}`);
      }
      if (tableNumber) {
        printer.println(`Table: ${tableNumber}`);
      }
      if (customerName) {
        printer.println(`Customer: ${customerName}`);
      }
      if (orderType) {
        printer.println(`Order Type: ${orderType}`);
      }
      if (paymentMethod) {
        printer.println(`Payment Method: ${paymentMethod}`);
      }
      printer.bold(false);
      if (
        printingData?.printingInvoiceDetailsResponseViewModels?.[0]
          ?.orderItemsDetailsResponseViewModels?.length > 0
      ) {
        printer.println(`------------------------------------------------`);
      }

      // Define the table header

      // Print each item in the order
      // Print each item in the order
      if (printingData?.isSendToKitchen) {
        if (
          printingData?.printingInvoiceDetailsResponseViewModels?.[0]
            ?.orderItemsDetailsResponseViewModels?.length > 0
        ) {
          printer.tableCustom([
            { text: "Items", width: 0.8, align: "LEFT", bold: true },
            { text: "Qty", width: 0.2, align: "LEFT", bold: true },
          ]);

          printingData?.printingInvoiceDetailsResponseViewModels?.[0]?.orderItemsDetailsResponseViewModels?.forEach(
            (item, index1) => {
              // Format the item name, quantity, and price
              const itemName = splitString(item.itemName, 20);
              const itemQuantity = item.quantity;

              printer.tableCustom([
                {
                  text: `${
                    index1 +
                    1 +
                    ")" +
                    itemName +
                    (item.description ? `[ ${item.description} ]` : ``)
                  }`,
                  width: 0.8,
                  align: "LEFT",
                },
                { text: itemQuantity, width: 0.2, align: "LEFT" },
              ]);
              item?.modifiers?.length > 0 &&
                item?.modifiers?.forEach((eachOrder, index) => {
                  printer.tableCustom([
                    {
                      text: ` -${eachOrder.modifierName}`,
                      width: 0.8,
                      align: "LEFT",
                    },
                    {
                      text: printingData?.isSendToKitchen
                        ? `${eachOrder.quantity}`
                        : `$${eachOrder.modifierPrice}`,
                      width: 0.2,
                      align: "LEFT",
                    },
                  ]);
                });
            }
          );
        }
        if (
          printingData?.printingInvoiceDetailsResponseViewModels?.[0]
            ?.setMenuOrderOrderDetailsResponseViewModels?.length > 0
        ) {
          printer.println(`------------------------------------------------`);
        }

        if (
          printingData?.printingInvoiceDetailsResponseViewModels?.[0]
            ?.setMenuOrderOrderDetailsResponseViewModels?.length > 0
        ) {
          printer.tableCustom([
            {
              text: "Combo pack Items",
              width: 0.8,
              align: "LEFT",
              bold: true,
            },
            { text: "Qty", width: 0.2, align: "LEFT", bold: true },
          ]);

          printingData?.printingInvoiceDetailsResponseViewModels?.[0]?.setMenuOrderOrderDetailsResponseViewModels?.forEach(
            (item, index1) => {
              // Format the item name, quantity, and price
              const itemName = item.setMenuName;
              const itemQuantity = item.quantity;

              printer.tableCustom([
                {
                  text: `${index1 + 1 + ")" + itemName}`,
                  width: 0.8,
                  align: "LEFT",
                },
                { text: itemQuantity, width: 0.2, align: "LEFT" },
              ]);

              item?.orderItemsDetailsResponseViewModels?.length > 0 &&
                item?.orderItemsDetailsResponseViewModels?.forEach(
                  (eachOrder, index) => {
                    printer.tableCustom([
                      {
                        text: ` -${eachOrder.itemName}`,
                        width: 0.9,
                        align: "LEFT",
                      },
                    ]);
                  }
                );
            }
          );
        }
        if (
          printingData?.printingInvoiceDetailsResponseViewModels?.[0]
            ?.description
        ) {
          printer.println(`------------------------------------------------`);
          printer.println(
            `Description: ${printingData?.printingInvoiceDetailsResponseViewModels?.[0]?.description}`
          );
        }
      } else {
        if (
          printingData?.printingInvoiceDetailsResponseViewModels?.[0]
            ?.orderItemsDetailsResponseViewModels?.length > 0
        ) {
          printer.tableCustom([
            { text: "Items", width: 0.7, bold: true, align: "LEFT" },
            { text: "Qty", width: 0.1, bold: true, align: "LEFT" },
            { text: "Price", width: 0.2, bold: true, align: "LEFT" },
          ]);

          printingData?.printingInvoiceDetailsResponseViewModels?.[0]?.orderItemsDetailsResponseViewModels?.forEach(
            (item, index1) => {
              // Format the item name, quantity, and price
              const itemName = splitString(item.itemName, 20);
              const itemQuantity = item.quantity;
              const itemPrice = `$${item.price}`;

              printer.tableCustom([
                {
                  text: `${index1 + 1 + ")" + itemName}`,
                  width: 0.7,
                },
                { text: itemQuantity, width: 0.1 },
                { text: itemPrice, width: 0.2 },
              ]);
              item?.modifiers?.length > 0 &&
                item?.modifiers?.forEach((eachOrder, index) => {
                  printer.tableCustom([
                    {
                      text: ` -${eachOrder.modifierName}`,
                      width: 0.8,
                      align: "LEFT",
                    },

                    {
                      text: printingData?.isSendToKitchen
                        ? `${eachOrder.quantity}`
                        : `$${eachOrder.modifierPrice}`,
                      width: 0.2,
                      align: "LEFT",
                    },
                  ]);
                });
            }
          );
          if (
            printingData?.printingInvoiceDetailsResponseViewModels?.[0]
              ?.setMenuOrderOrderDetailsResponseViewModels?.length > 0
          ) {
            printer.tableCustom([
              { text: "Combo Pack Items", bold: true, align: "LEFT" },
              { text: "Qty", bold: true, align: "LEFT" },
              { text: "Price", bold: true, align: "LEFT" },
            ]);
            printingData?.printingInvoiceDetailsResponseViewModels?.[0]?.setMenuOrderOrderDetailsResponseViewModels?.forEach(
              (item, index1) => {
                // Format the item name, quantity, and price
                const itemName = splitString(item.setMenuName, 20);
                const itemQuantity = item.quantity;
                const itemPrice = `$${item.price}`;

                printer.tableCustom([
                  { text: `${index1 + 1 + ")" + itemName}`, width: 0.6 },
                  { text: itemQuantity, width: 0.2 },

                  { text: itemPrice, width: 0.2 },
                ]);
              }
            );
          }
        }
      }

      // Function to split a string into multiple lines
      function splitString(str, len) {
        const regex = new RegExp(`.{1,${len}}`, "g");
        const lines = str.match(regex);
        return lines || [];
      }
      // Print the table footer
      // Print total price
      printer.println(`------------------------------------------------`);

      printer.alignRight();
      if (!printingData?.isSendToKitchen) {
        if (
          parseFloat(
            printingData?.printingInvoiceDetailsResponseViewModels?.[0]
              ?.tipAmount
          ) > 0.0
        ) {
          printer.println(
            `   Tip Amount:    $${printingData?.printingInvoiceDetailsResponseViewModels?.[0]?.tipAmount}`
          );
        }
        if (
          parseFloat(
            printingData?.printingInvoiceDetailsResponseViewModels?.[0]
              ?.creditCardSurchargeAmountWithTax
          ) > 0.0
        ) {
          if (
            printingData?.printingInvoiceDetailsResponseViewModels?.[0]
              ?.taxExclusiveInclusiveType == "TaxInclusive"
          ) {
            printer.println(
              `   Credit Card Surcharge Amount:    $${printingData?.printingInvoiceDetailsResponseViewModels?.[0]?.creditCardSurchargeAmountWithTax}`
            );
          } else {
            printer.println(
              `   Credit Card Surcharge Amount:    $${printingData?.printingInvoiceDetailsResponseViewModels?.[0]?.creditCardSurchargeAmount}`
            );
          }
        }
        if (
          parseFloat(
            printingData?.printingInvoiceDetailsResponseViewModels?.[0]
              ?.publicHolidaySurCharge
          ) > 0.0
        ) {
          if (
            printingData?.printingInvoiceDetailsResponseViewModels?.[0]
              ?.taxExclusiveInclusiveType == "TaxInclusive"
          ) {
            printer.println(
              `   Holiday Surcharge Amount:    $${printingData?.printingInvoiceDetailsResponseViewModels?.[0]?.publicHolidaySurChargeWithTax}`
            );
          } else {
            printer.println(
              `   Holiday Surcharge Amount:    $${printingData?.printingInvoiceDetailsResponseViewModels?.[0]?.publicHolidaySurCharge}`
            );
          }
        }
        if (
          parseFloat(
            printingData?.printingInvoiceDetailsResponseViewModels?.[0]
              ?.discount
          ) > 0.0
        ) {
          if (
            printingData?.printingInvoiceDetailsResponseViewModels?.[0]
              ?.taxExclusiveInclusiveType == "TaxInclusive"
          ) {
            printer.println(
              `   Discount Amount:    $${printingData?.printingInvoiceDetailsResponseViewModels?.[0]?.discountWithTax}`
            );
          } else {
            printer.println(
              `   Discount Amount:    $${printingData?.printingInvoiceDetailsResponseViewModels?.[0]?.discount}`
            );
          }
        }
        printer.println(`   Tax:    $${taxAmount}`);
        printer.println(`   Total:  $${totalPrice}`);
      }
      printer.newLine();
      printer.newLine();
      printer.alignCenter();
      printer.bold(true);
      if (printingData?.isPrintFromPayScreen) {
        printer.println(`*CreditcardSurchage may apply`);
        printer.println(`*Redeem code discount may apply`);
      }
      printer.bold(false);

      printer.newLine();
      printer.newLine();
      printer.alignCenter();

      printer.bold(true);
      printer.println(`Thank you for your visit !`);

      printer.setTextNormal();
      printer.println(
        `ABN: ${printingData?.printingInvoiceDetailsResponseViewModels?.[0]?.abnNumber}`
      );
      printer.bold(false);

      // Print and disconnect the printer
      printer.cut();
      printer.execute();
      console.error("Print done!");
      res.status(200).json({ success: "this is working" });
    } catch (error) {
      console.log("Print failed:", error);
      res.status(400).json({ message: "" });
    }
  });

  app.listen(7823, () => {});
  module.exports = app;
})();
